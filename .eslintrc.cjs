module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  plugins: ["react"],
  extends: ["eslint:recommended", "plugin:react/recommended"],
  settings: {
    react: { version: "detect" },
  },
  parser: "babel-eslint",
  rules: {
    "react/prop-types": "off",
  },
};
