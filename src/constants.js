const meats = ["chicken", "turkey", "bacon", "patty"];
const ingredients = ["tomato", "lettuce", "cheese", ...meats];
const dispensers = [...ingredients, "plate"];

const colors = {
  lettuce: [81, 198, 192],
  tomato: [13, 249, 211],
  plate: [84, 6, 228],
  chicken: [20, 99, 232],
  cheese: [42, 192, 243],
  turkey: [35, 72, 199],
  bacon: [12, 213, 141],
  patty: [357, 112, 241]
};

const orderDuration = 1000 * 30;

export default {
  dispensers,
  orderDuration,
  needsCooking: meats,
  ingredients,
  colors,
};
