import fs from "fs";

import { updateState } from "./utils.js";
import constants from "./constants.js";

function send(client, key, value) {
  client.send(JSON.stringify([key, value]));
}

export default class Game {
  constructor(gameId, mapName) {
    this.gameId = gameId;
    this.clients = [];
    const mapFile = mapName ? `assets/maps/${mapName}.json` : "assets/default.json";
    const map = JSON.parse(fs.readFileSync(mapFile));
    this.state = { type: "s", score: 0, players: [], pieces: [], map, orders: [], timer: 1000 * 60 * 5, gameOver: false };
    this.timeToNextOrder = 0;

    setInterval(() => {
      this.updateClientLiveliness();
      this.updatePlayerLiveliness();
      this.updateGameTimer();
      this.updateOrders();
    }, 1000);

    setInterval(() => {
      this.updatePieces();
      this.updatePlayerTimers();
    }, 100);
  }
  updateClientLiveliness() {
    for (let i = 0; i < this.clients.length; i++) {
      const client = this.clients[i];
      if (!client.a) continue;
      client.a = Date.now() - client.lastPong <= 2000;
      if (!client.a) continue;
      client.ping();
    }
  }
  updatePlayerLiveliness() {
    for (let i = 0; i < this.state.players.length; i++) {
      const player = this.state.players[i];
      if (!player || !player.a) continue;
      player.a = this.clients.some((client) => client.playerId === i && client.a);
      if (!player.a) {
        const playerMessage = JSON.stringify(["players " + i + " a", false]);
        for (const client of this.clients) {
          if (client.playerId === i) continue;
          client.send(playerMessage);
        }
      }
    }
  }
  updatePlayerTimers() {
    for (let i = 0; i < this.state.players.length; i++) {
      const player = this.state.players[i];
      if (!player || !player.a || player.timer === null || player.timer <= 0) continue;
      player.timer -= 100;
      const playerMessage = JSON.stringify(["players " + i + " timer", player.timer]);
      for (const client of this.clients) {
        client.send(playerMessage);
      }
    }
  }
  updateGameTimer() {
    if (this.state.timer <= 0) {
      for (const client of this.clients) {
        send(client, "gameOver", true);
      }
    } else {
      this.state.timer -= 1000;
      for (const client of this.clients) {
        send(client, "timer", this.state.timer);
      }
    }
  }
  updateOrders() {
    for (let i = 0; i < this.state.orders.length; i++) {
      const order = this.state.orders[i];
      if (order === null) continue;

      order.timer -= 1000;
      if (order.timer <= 0) {
        this.state.orders[i] = null;
        for (const client of this.clients) {
          send(client, "orders " + i, null);
        }
      } else {
        for (const client of this.clients) {
          send(client, "orders " + i + " timer", order.timer);
        }
      }
    }

    this.timeToNextOrder -= 1000;
    const numActiveOrders = this.state.orders.reduce((a, o) => (o === null ? a : a + 1), 0);
    if (this.timeToNextOrder <= 0 && numActiveOrders < 5) {
      const i = this.state.orders.length;
      const recipes = this.state.map.recipes;
      const order = JSON.parse(JSON.stringify(recipes[Math.floor(Math.random() * recipes.length)]));
      order.timer = constants.orderDuration;
      this.state.orders.push(order);
      for (const client of this.clients) {
        send(client, "orders " + i, order);
      }
      this.timeToNextOrder = 1000 * 10;
    }
  }
  updatePieces() {
    for (let r = 0; r < this.state.map.tiles.length; r++) {
      for (let c = 0; c < this.state.map.tiles[0].length; c++) {
        const tile = this.state.map.tiles[r][c];
        if (tile.type === null || !constants.dispensers.includes(tile.type) || tile.c !== null) continue;

        tile.ttp -= 100;
        if (tile.ttp <= 0) {
          const i = this.state.pieces.length;
          const piece = { x: c * 48 + 24, y: r * 48 + 24, type: tile.type, state: null, c: null, tile: [r, c] };
          this.state.pieces.push(piece);
          tile.c = i;
          for (const client of this.clients) {
            send(client, "pieces " + i, piece);
            send(client, "map tiles " + r + " " + c + " c", i);
          }
          tile.ttp = 1000;
        }
      }
    }
  }
  registerPlayer(client, id) {
    if (id === null) {
      client.playerId = this.state.players.length;
      this.state.players.push({ a: true, timer: null, t: { x: 24, y: 24, r: 0 } });
    } else {
      client.playerId = id;
      if (this.state.players[id] === undefined) {
        this.state.players[id] = { a: true, timer: null, t: { x: 24, y: 24, r: 0 } };
      } else {
        this.state.players[id].a = true;
      }
    }

    client.on("pong", () => {
      client.lastPong = Date.now();
    });

    client.send(JSON.stringify({ type: "id", id: client.playerId }));

    client.send(JSON.stringify(this.state));

    this.clients.push(client);

    const playerMessage = JSON.stringify(["players " + client.playerId, this.state.players[client.playerId]]);
    for (const otherClient of this.clients) {
      if (otherClient === client || !client.a) continue;
      otherClient.send(playerMessage);
    }
  }
  addClient(client) {
    client.lastPong = Date.now();
    client.a = true;

    client.on("message", (message) => {
      const data = JSON.parse(message);
      if (Array.isArray(data)) {
        updateState(this.state, data);
        for (const otherClient of this.clients) {
          if (otherClient === client || !client.a) continue;
          otherClient.send(message);
        }
      } else {
        if (data.type === "id") {
          this.registerPlayer(client, data.id);
        } else if (data.type === "save_map") {
          const [w, h] = data.size;
          const tiles = [];
          const map = {tiles};
          for (let r = 0; r < h; r++) {
            if (!tiles[r]) {
              tiles[r] = [];
            }
            for (let c = 0; c < w; c++) {
              let mapTile;
              if (this.state.map.tiles[r] && this.state.map.tiles[r][c]) {
                mapTile = this.state.map.tiles[r][c];
              }
              tiles[r][c] = { type: mapTile ? mapTile.type : null, ttp: 1000, c: null };
            }
          }
          map.size = data.size;
          map.recipes = data.recipes;
          if (!fs.existsSync("assets/maps")) {
            fs.mkdirSync("assets/maps");
          }
          fs.writeFileSync(`assets/maps/${data.name}.json`, JSON.stringify(map, null, 2));
        }
      }
    });
  }
}
