import express from "express";
import ws from "ws";
import http from "http";

import Game from "./game.js";

const app = express();

app.use(express.static("."));

const server = http.createServer(app);

const wss = new ws.Server({ server });

const games = {};

wss.on("connection", function (client, req) {
  const params = new URLSearchParams(req.url.split("?")[1]);
  const gameId = params.get("id");
  const map = params.get("map");

  if (!games[gameId]) {
    games[gameId] = new Game(gameId, map);
  }
  games[gameId].addClient(client);
});

server.listen(3000, () => console.log("listening on 3000"));
