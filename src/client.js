import p5 from "p5";
import React from "react";
import ReactDOM from "react-dom";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import { collideLineCircle, collideRectCircle } from "p5collide";
import { v4 as uuidv4 } from "uuid";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faVolumeMute, faVolumeUp } from "@fortawesome/free-solid-svg-icons";

import avatarImages from "../assets/avatar-images.js";
import { updateState, formatTimer } from "./utils.js";
import constants from "./constants.js";

const recipeImages = {};
const renderPieceImage = (() => {
  let p;
  let c;
  const cp = new Promise((resolve) => {
    new p5((_p) => {
      p = _p;
      p.setup = () => {
        c = p.createCanvas(32, 32).elt;
        p.colorMode(p.HSB, 360, 255, 255);
        c.style.display = "none";
        resolve();
      };
    });
  });
  return async (piece) => {
    await cp;
    p.clear();
    p.push();
    p.translate(16, 16);
    Client.renderPiece(p, piece, true);
    p.pop();
    return new Promise((resolve) => c.toBlob(resolve));
  };
})();
async function generateRecipeImages(recipes) {
  for (const recipe of recipes) {
    recipeImages[recipe.name] = URL.createObjectURL(
      await renderPieceImage({ type: "plate", state: recipe.ingredients })
    );
  }
}

const audioContext = new AudioContext();
audioContext.suspend();
const gainNode = audioContext.createGain();
gainNode.gain.value = 0.5;
gainNode.connect(audioContext.destination);
const sounds = {};
function loadSound(url) {
  return fetch(url).then(async (r) => {
    const arrayBuffer = await r.arrayBuffer();
    return await audioContext.decodeAudioData(arrayBuffer);
  });
}
function playSound(name) {
  const audioSource = audioContext.createBufferSource();
  audioSource.connect(gainNode);
  audioSource.buffer = sounds[name];
  audioSource.start(0);
}

class Client {
  static TS = 48;
  static HTS = Client.TS / 2;
  static actionDuration = 1000;
  static offset = new p5.Vector();
  static vel = new p5.Vector();
  static stateToState = {
    chopping: "chopped",
    cooking: "cooked",
  };
  static ingredientsMatch(orderIngredients, plateIngredients) {
    if (orderIngredients.length !== plateIngredients.length) return false;
    orderIngredients.sort();
    plateIngredients.sort();
    for (let i = 0; i < orderIngredients.length; i++) {
      if (orderIngredients[i] !== plateIngredients[i]) return false;
    }
    return true;
  }
  static renderPiece(p, piece, isHighlighted) {
    const pieceColor = constants.colors[piece.type];
    p.stroke("black");
    p.strokeWeight(1);
    p.fill(isHighlighted ? p.color(pieceColor) : p.color(pieceColor[0], pieceColor[1], pieceColor[2] * 0.8));
    p.circle(0, 0, 24);

    p.noStroke();
    if (piece.type === "plate" && piece.state !== null) {
      for (let j = 0; j < piece.state.length; j++) {
        const type = piece.state[j];
        p.fill(constants.colors[type]);
        p.circle(-6 + (j % 2) * 12, -6 + Math.floor(j / 2) * 12, 8);
      }
    } else if (piece.state === "chopped" || piece.state === "cooked") {
      p.fill(piece.state === "chopped" ? 255 : 0);
      p.rect(-8, -7, 16, 2);
      p.rect(-10, -1, 20, 2);
      p.rect(-8, 5, 16, 2);
    }
  }

  constructor() {
    this.state = null;
    this.avatars = [];
    this.tiles = {};
    this.selectedTile = "counter";
    this.playerId = null;
    this.me = { a: true, state: null, timer: null, t: { x: Client.HTS, y: Client.HTS, r: 0 }, c: null };
    this.connect().then((state) => {
      this.initSketch(state.map.size[0], state.map.size[1]);
    });
    this.hostMode = new URLSearchParams(location.search).get("host") !== null;
  }
  connect() {
    return new Promise((resolve) => {
      const url = new URL(location.href);
      url.protocol = "ws:";
      const ws = new WebSocket(url.toString());
      this.ws = ws;

      ws.addEventListener("message", async (event) => {
        const data = JSON.parse(event.data);
        if (Array.isArray(data)) {
          updateState(this.state, data);
          this.renderUI();
        } else {
          switch (data.type) {
            case "id":
              this.playerId = data.id;
              localStorage.setItem("id", this.playerId);
              break;
            case "s":
              this.state = window.state = data;
              resolve(this.state);
              await generateRecipeImages(this.state.map.recipes);
              this.renderUI();
              break;
          }
        }
      });

      ws.addEventListener("open", () => {
        const storedId = localStorage.getItem("id");
        this.playerId = storedId === null ? null : Number(storedId);
        ws.send(JSON.stringify({ type: "id", id: this.playerId }));
      });

      ws.addEventListener("close", () => {
        console.log("Connection lost. Reloading");
        const checkInterval = setInterval(async () => {
          const resp = await fetch(location.href, { method: "HEAD" });
          if (resp.status === 200) {
            clearInterval(checkInterval);
            location.reload();
          }
        }, 100);
      });
    });
  }
  initSketch(w, h) {
    new p5((p) => {
      this.sketch = p;
      p.setup = () => {
        const canvas = p.createCanvas(w * Client.TS, h * Client.TS);
        canvas.parent("sketch");
        p.colorMode(p.HSB, 360, 255, 255);
        p.imageMode(p.CENTER);
        p.textAlign(p.CENTER, p.CENTER);
        p.textSize(11);
      };
      p.draw = this.update;
      p.mousePressed = this.mousePressed;
    });
  }
  renderUI = () => {
    ReactDOM.render(
      <ClientUI
        hostMode={this.hostMode}
        gameState={this.state}
        onSelectedTileChanged={this.setSelectedTile}
        onSaveMap={this.saveMap}
      />,
      document.getElementById("root")
    );
  };
  saveMap = (name, size, recipes) => {
    this.ws.send(JSON.stringify({ type: "save_map", name, size, recipes }));
  };
  setSelectedTile = (tile) => {
    this.selectedTile = tile;
  };
  mousePressed = (e) => {
    if (e.target !== this.sketch.canvas) return;
    if (!this.hostMode) return;
    const p = this.sketch;
    const r = p.floor(p.mouseY / Client.TS);
    const c = p.floor(p.mouseX / Client.TS);
    const [w, h] = this.state.map.size;
    if (r < 0 || r >= h || c < 0 || c >= w) return;
    const type = p.mouseButton === p.LEFT ? this.selectedTile : null;
    const ttp = type ? 1000 : null;
    const tile = { type, ttp, c: null };
    this.state.map.tiles[r][c] = tile;
    this.send("map tiles " + r + " " + c, tile);
  };
  getPositionInFrontOfMe() {
    const offset = Client.offset;
    offset.set(0, 32).rotate(this.me.t.r).add(this.me.t.x, this.me.t.y);
    return [offset.x, offset.y];
  }
  positionPieceInFrontOf(piece, player) {
    const offset = Client.offset;
    offset.set(0, 32).rotate(player.t.r).add(player.t.x, player.t.y);
    piece.x = offset.x;
    piece.y = offset.y;
    this.sketch.translate(offset.x, offset.y);
  }
  isPieceInFrontOfMe(piece) {
    const me = this.me;
    const offset = Client.offset;
    const width = 10;
    const length = 50;
    const { x: l1x1, y: l1y1 } = offset.set(0, 0).add(me.t.x, me.t.y);
    const { x: l1x2, y: l1y2 } = offset.set(0, length).rotate(me.t.r).add(me.t.x, me.t.y);
    const { x: l2x1, y: l2y1 } = offset.set(-width, 0).rotate(me.t.r).add(me.t.x, me.t.y);
    const { x: l2x2, y: l2y2 } = offset.set(-width, length).rotate(me.t.r).add(me.t.x, me.t.y);
    const { x: l3x1, y: l3y1 } = offset.set(width, 0).rotate(me.t.r).add(me.t.x, me.t.y);
    const { x: l3x2, y: l3y2 } = offset.set(width, length).rotate(me.t.r).add(me.t.x, me.t.y);
    return (
      collideLineCircle(l1x1, l1y1, l1x2, l1y2, piece.x, piece.y, 20) ||
      collideLineCircle(l2x1, l2y1, l2x2, l2y2, piece.x, piece.y, 20) ||
      collideLineCircle(l3x1, l3y1, l3x2, l3y2, piece.x, piece.y, 20)
    );
  }
  getTileInFront() {
    const [x, y] = this.getPositionInFrontOfMe();
    const c = Math.floor(x / Client.TS);
    const r = Math.floor(y / Client.TS);
    return this.state.map.tiles[r][c];
  }
  getPieceInFront() {
    for (let i = 0; i < this.state.pieces.length; i++) {
      const piece = this.state.pieces[i];
      if (piece === null) continue;
      if (this.isPieceInFrontOfMe(piece)) {
        return [piece, i];
      }
    }
    return [null, null];
  }
  getAvatar(i) {
    if (!this.avatars[i]) {
      this.avatars[i] = this.sketch.loadImage("assets/images/avatars/" + avatarImages[i], (image) =>
        image.resize(32, 32)
      );
    }
    return this.avatars[i];
  }
  getTile(type) {
    if (!this.tiles[type]) {
      this.tiles[type] = this.sketch.loadImage(`assets/images/tiles/${type}.png`, (image) => image.resize(48, 48));
    }
    return this.tiles[type];
  }
  blocked = (r, c, x, y) => {
    const [w, h] = this.state.map.size;
    const outOfBounds = r < 0 || r >= h || c < 0 || c >= w;
    if (outOfBounds || this.state.map.tiles[r][c].type !== null) {
      return collideRectCircle(c * Client.TS, r * Client.TS, Client.TS, Client.TS, x, y, 32);
    }
    return false;
  };
  send = (key, data) => {
    this.ws.send(JSON.stringify([key, data]));
  };
  renderMap() {
    const p = this.sketch;
    const state = this.state;
    const TS = Client.TS;
    const [w, h] = state.map.size;
    for (let r = 0; r < h; r++) {
      for (let c = 0; c < w; c++) {
        let type = null;
        if (state.map.tiles[r] && state.map.tiles[r][c]) {
          type = state.map.tiles[r][c].type;
        }

        const isDispenser = constants.dispensers.includes(type);
        let tile;
        if (type === null) tile = "tile";
        else if (isDispenser) tile = "counter";
        else tile = type;
        p.image(this.getTile(tile), c * TS + TS / 2, r * TS + TS / 2);

        if (type !== null && type !== "counter") {
          p.fill("black");
          p.strokeJoin(p.ROUND);
          p.strokeWeight(5);
          p.stroke("white");
          p.textAlign(p.CENTER, p.TOP);
          p.text(type, c * TS + TS / 2, r * TS + 5);
        }
      }
    }
  }
  renderPlayers() {
    const p = this.sketch;
    const state = this.state;
    const me = this.me;
    const id = this.playerId;
    for (let i = 0; i < state.players.length; i++) {
      const player = i === id ? me : state.players[i];
      if (!player || !player.a) continue;
      p.push();
      p.translate(player.t.x, player.t.y);
      p.rotate(player.t.r);
      p.image(this.getAvatar(i), 0, 0);
      if (player.timer !== null) {
        p.noStroke();
        p.fill("black");
        p.rect(-11, -3 - 22, 22, 6);
        p.fill("lightgreen");
        p.rect(-10, -2 - 22, (1 - player.timer / Client.actionDuration) * 20, 4);
      }
      p.pop();
    }
  }
  renderPieces() {
    const p = this.sketch;
    const state = this.state;
    const me = this.me;
    const id = this.playerId;
    for (let i = 0; i < state.pieces.length; i++) {
      const piece = state.pieces[i];
      if (piece === null) continue;
      p.push();
      if (me.c === i) {
        this.positionPieceInFrontOf(piece, me);
      } else if (piece.c !== null && piece.c !== id) {
        this.positionPieceInFrontOf(piece, state.players[piece.c]);
      } else {
        p.translate(piece.x, piece.y);
      }
      Client.renderPiece(p, piece, this.isPieceInFrontOfMe(piece));
      p.pop();
    }
  }
  pieceOnTile(r, c) {
    for (let i = 0; i < this.state.pieces.length; i++) {
      const piece = this.state.pieces[i];
      if (piece === null || piece.tile === null) continue;
      if (piece.tile[0] === r && piece.tile[1] === c) return [piece, i];
    }
    return [null, null];
  }
  dropPiece() {
    const me = this.me;
    const state = this.state;
    const send = this.send;
    const TS = Client.TS;
    const HTS = Client.HTS;
    const [x, y] = this.getPositionInFrontOfMe();
    const c = Math.floor(x / TS);
    const r = Math.floor(y / TS);
    const tile = state.map.tiles[r][c];
    const piece = state.pieces[me.c];
    if (tile.type !== null) {
      if (tile.type === "dropoff" && piece.type === "plate" && piece.state !== null) {
        let matchingOrderIndex = null;
        for (let i = 0; i < state.orders.length; i++) {
          const order = state.orders[i];
          if (order === null) continue;
          if (Client.ingredientsMatch(order.ingredients, piece.state)) {
            matchingOrderIndex = i;
            break;
          }
        }
        if (matchingOrderIndex !== null) {
          state.orders[matchingOrderIndex] = null;
          send("orders " + matchingOrderIndex, null);
          state.pieces[me.c] = null;
          send("pieces " + me.c, null);
          state.score++;
          send("score", "++");
          this.renderUI();
          me.c = null;
        }
      } else if (tile.type === "trash") {
        state.pieces[me.c] = null;
        send("pieces " + me.c, null);
        me.c = null;
      } else {
        const [pieceOnTile, i] = this.pieceOnTile(r, c);
        const needsCooking = constants.needsCooking.includes(piece.type);
        if (pieceOnTile === null) {
          const tx = c * TS + HTS;
          const ty = r * TS + HTS;
          piece.x = tx;
          piece.y = ty;
          piece.tile = [r, c];
          send("pieces " + me.c, { x: tx, y: ty, state: piece.state, type: piece.type, c: null, tile: piece.tile });
          me.c = null;
        } else if (
          pieceOnTile.type === "plate" &&
          ((needsCooking && piece.state === "cooked") || (!needsCooking && piece.state === "chopped"))
        ) {
          let pieceState = pieceOnTile.state;
          if (pieceState === null) {
            pieceState = [];
          }
          pieceState.push(piece.type);
          state.pieces[i].state = pieceState;
          send("pieces " + i + " state", pieceState);
          state.pieces[me.c] = null;
          send("pieces " + me.c, null);
          me.c = null;
        }
      }
    } else {
      send("pieces " + me.c, { x, y, state: piece.state, type: piece.type, c: null, tile: null });
      me.c = null;
    }
  }
  pickUpPiece() {
    const state = this.state;
    const id = this.playerId;
    for (let i = 0; i < state.pieces.length; i++) {
      const piece = state.pieces[i];
      if (piece == null) continue;
      if (this.isPieceInFrontOfMe(piece)) {
        this.me.c = i;
        if (piece.tile !== null) {
          const [r, c] = piece.tile;
          piece.c = id;
          piece.tile = null;
          state.map.tiles[r][c].c = null;
          this.send("map tiles " + r + " " + c + " c", null);
          this.send("pieces " + i, { ...piece, c: id, tile: null });
        } else {
          this.send("pieces " + i + " c", id);
        }
        break;
      }
    }
  }
  movePlayer() {
    const p = this.sketch;
    const me = this.me;
    const vel = Client.vel;
    const blocked = this.blocked;

    const cr = Math.floor(me.t.y / Client.TS);
    const cc = Math.floor(me.t.x / Client.TS);

    let keyWasDown = false;
    vel.x = vel.y = 0;
    if (p.keyIsDown(87)) {
      vel.y = -1;
      keyWasDown = true;
    } else if (p.keyIsDown(83)) {
      vel.y = 1;
      keyWasDown = true;
    }
    if (p.keyIsDown(65)) {
      vel.x = -1;
      keyWasDown = true;
    } else if (p.keyIsDown(68)) {
      vel.x = 1;
      keyWasDown = true;
    }
    vel.normalize().mult(2);

    const cx = me.t.x;
    const cy = me.t.y;
    const x = cx + vel.x;
    const y = cy + vel.y;
    const r = vel.heading() - Math.PI / 2;

    if (
      (vel.y > 0 && (blocked(cr + 1, cc - 1, cx, y) || blocked(cr + 1, cc, cx, y) || blocked(cr + 1, cc + 1, cx, y))) ||
      (vel.y < 0 && (blocked(cr - 1, cc - 1, cx, y) || blocked(cr - 1, cc, cx, y) || blocked(cr - 1, cc + 1, cx, y)))
    ) {
      vel.y = 0;
    }

    if (
      (vel.x > 0 && (blocked(cr - 1, cc + 1, x, cy) || blocked(cr, cc + 1, x, cy) || blocked(cr + 1, cc + 1, x, cy))) ||
      (vel.x < 0 && (blocked(cr - 1, cc - 1, x, cy) || blocked(cr, cc - 1, x, cy) || blocked(cr + 1, cc - 1, x, cy)))
    ) {
      vel.x = 0;
    }

    if (vel.x || vel.y) {
      if (vel.x !== 0) {
        me.t.x = x;
      }
      if (vel.y !== 0) {
        me.t.y = y;
      }
      me.t.r = r;
      this.send("players " + this.playerId + " t", { x: me.t.x, y: me.t.y, r });
      if (this.me.timer !== null) {
        this.me.timer = null;
        this.state.players[this.playerId].timer = null;
        this.send("players " + this.playerId + " timer", null);
      }
    } else if (keyWasDown) {
      if (me.t.r !== r && this.me.timer !== null) {
        this.me.timer = null;
        this.state.players[this.playerId].timer = null;
        this.send("players " + this.playerId + " timer", null);
      }
      me.t.r = r;
      this.send("players " + this.playerId + " t r", r);
    }
  }
  update = (() => {
    let spaceIsUp = true;
    return () => {
      this.sketch.background(255);
      if (this.state === null || this.playerId === null) return;

      this.me.timer = this.state.players[this.playerId].timer;

      if (this.me.timer !== null && this.me.timer <= 0) {
        this.me.timer = null;
        this.state.players[this.playerId].timer = null;
        this.send("players " + this.playerId + " timer", null);
        const [piece, i] = this.getPieceInFront();

        piece.state = Client.stateToState[this.me.state];
        this.send("pieces " + i + " state", piece.state);
        this.me.state = null;
      }

      this.renderMap();

      this.renderPlayers();

      this.renderPieces();

      if (this.sketch.keyIsDown(32)) {
        if (spaceIsUp) {
          spaceIsUp = false;
          if (this.me.c !== null) {
            this.dropPiece();
          } else {
            this.pickUpPiece();
          }
        }
      } else {
        spaceIsUp = true;
      }

      if (this.me.c === null && this.sketch.keyIsDown(69)) {
        const tile = this.getTileInFront();
        const [piece] = this.getPieceInFront();
        if (piece !== null && piece.type !== "plate" && tile.type === "chop") {
          this.me.state = "chopping";
          this.me.timer = Client.actionDuration;
          this.send("players " + this.playerId + " timer", Client.actionDuration);
        }
        if (
          piece !== null &&
          piece.type !== "plate" &&
          constants.needsCooking.includes(piece.type) &&
          piece.state === "chopped" &&
          tile.type === "stove"
        ) {
          this.me.state = "cooking";
          this.me.timer = Client.actionDuration;
          this.send("players " + this.playerId + " timer", Client.actionDuration);
        }
      }

      this.movePlayer();
    };
  })();
}

function formatRecipes(recipes) {
  return recipes.map((r) => `${r.name}:${r.ingredients.join(", ")}`).join("\n");
}

function parseRecipes(recipes) {
  return recipes.split("\n").map((line) => {
    const parts = line.split(":");
    return {
      name: parts[0].trim(),
      ingredients: parts[1].split(",").map((i) => i.trim()),
    };
  });
}

class ClientUI extends React.Component {
  static tiles = ["counter", "chop", "stove", "trash", "plate", "dropoff", ...constants.ingredients];
  constructor(props) {
    super(props);
    this.state = {
      selected: "counter",
      animateScore: false,
      muted: true,
      showRecipes: false,
      recipes: formatRecipes(props.gameState.map.recipes),
      mapSize: props.gameState.map.size.join("x"),
    };
    this.lastScore = 0;
    this.animateScore = false;
  }
  setSelected(tile) {
    this.props.onSelectedTileChanged(tile);
    this.setState({ selected: tile });
  }
  flipScoreAnimation = () => {
    this.animateScore = false;
  };
  toggleAudioContext = () => {
    this.setState((state) => {
      if (state.muted) {
        audioContext.resume();
      } else {
        audioContext.suspend();
      }
      state.muted = !state.muted;
      return state;
    });
  };
  render() {
    if (this.props.gameState.score !== this.lastScore) {
      playSound("caching");
      this.animateScore = true;
      setTimeout(this.flipScoreAnimation, 300);
    }
    this.lastScore = this.props.gameState.score;
    return (
      <div>
        <div className="container">
          <div className="sidebar">
            {this.props.hostMode && (
              <div>
                {ClientUI.tiles.map((tile) => (
                  <div
                    key={tile}
                    style={{ border: this.state.selected === tile ? "1px solid black" : "none" }}
                    onClick={() => this.setSelected(tile)}
                  >
                    {tile}
                  </div>
                ))}
                <button
                  onClick={() => {
                    localStorage.clear();
                    location.reload();
                  }}
                >
                  reset
                </button>
                <br />
                <button
                  onClick={() => {
                    localStorage.clear();
                    location.href = "/?host";
                  }}
                >
                  new
                </button>
                <br />
                <input
                  size="4"
                  value={this.state.mapSize}
                  onChange={(e) => this.setState({ mapSize: e.target.value })}
                />
                <br />
                <button onClick={() => this.setState({ showRecipes: true })}>recipes</button>
                <br />
                <button
                  onClick={() =>
                    this.props.onSaveMap(
                      prompt("map name"),
                      this.state.mapSize.split("x").map((s) => parseInt(s, 10)),
                      parseRecipes(this.state.recipes)
                    )
                  }
                >
                  save
                </button>
              </div>
            )}
            <div>
              <FontAwesomeIcon
                className="mute"
                onClick={this.toggleAudioContext}
                icon={this.state.muted ? faVolumeMute : faVolumeUp}
                title={this.state.muted ? "unmute" : "mute"}
              />
              <div className="timer">{formatTimer(this.props.gameState.timer)}</div>
              <CSSTransition in={this.animateScore} timeout={300} classNames="score">
                <div className="score">{this.props.gameState.score * 10}</div>
              </CSSTransition>
              <TransitionGroup>
                {this.props.gameState.orders &&
                  this.props.gameState.orders.map((order, i) => {
                    return (
                      order && (
                        <CSSTransition key={i} timeout={300} classNames="order">
                          <div className="order">
                            <img src={recipeImages[order.name]} />
                            <div className="name">{order.name}</div>
                            <div className="progress">
                              <div
                                className="progress-bar"
                                style={{
                                  backgroundColor: `hsl(${
                                    (order.timer / constants.orderDuration) * 120
                                  }deg, 100%, 50%)`,
                                  width: (order.timer / constants.orderDuration) * 100 + "%",
                                }}
                              ></div>
                            </div>
                          </div>
                        </CSSTransition>
                      )
                    );
                  })}
              </TransitionGroup>
            </div>
          </div>
        </div>
        {this.state.showRecipes && (
          <div className="recipes">
            <textarea
              value={this.state.recipes}
              onChange={(e) => this.setState({ recipes: e.target.value })}
            ></textarea>
            <button onClick={() => this.setState({ showRecipes: false })}>close</button>
          </div>
        )}
        {!this.props.hostMode && this.props.gameState.gameOver && (
          <div className="game-over">
            Game Over!
            <br />
            Score: {this.props.gameState.score * 10}
          </div>
        )}
      </div>
    );
  }
}

(async () => {
  const gameId = new URLSearchParams(location.search).get("id");
  if (gameId === null) {
    const url = new URL(location.href);
    url.searchParams.set("id", uuidv4());
    location.href = url.toString();
    return;
  }

  document.body.addEventListener("contextmenu", (e) => e.preventDefault());

  sounds["caching"] = await loadSound("assets/sounds/caching.wav");

  new Client();
})();
