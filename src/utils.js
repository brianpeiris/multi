export function updateState(state, data) {
  const [key, value] = data;
  let curr = state;
  const parts = key.split(" ");
  for (let i = 0; i < parts.length - 1; i++) {
    curr = curr[parts[i]];
  }
  if (value === "++") {
    curr[parts[parts.length - 1]]++;
  } else {
    curr[parts[parts.length - 1]] = value;
  }
}

export function formatTimer(timer) {
  const minutes = timer / 1000 / 60;
  const minutesRounded = Math.floor(minutes);
  const seconds = String(Math.floor((minutes - minutesRounded) * 60)).padStart(2, "0");
  return `${minutesRounded}:${seconds}`;
}
